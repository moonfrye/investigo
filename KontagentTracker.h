//
//  KontagentTracker.h
//  Memories
//
//  Created by Aaron Wilson on 11/14/13.
//  Copyright (c) 2013 Moonfrye, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LolayBaseTracker.h"
#import "Kontagent/Kontagent.h"

@interface KontagentTracker : LolayBaseTracker

- (id) initWithKey:(NSString*) key;
- (id) initWithKey:(NSString*) key version:(NSString*) version;
- (KTParamMap*) buildParameters:(NSDictionary*) parameters;

@end
