//
//  KontagentTracker.m
//  Memories
//
//  Created by Aaron Wilson on 11/14/13.
//  Copyright (c) 2013 Moonfrye, Inc. All rights reserved.
//

#import "KontagentTracker.h"
#import "Kontagent/Kontagent.h"
#import "JSONKit.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@interface KontagentTracker ()

@property (nonatomic, strong, readwrite) NSMutableDictionary* globalParametersValue;

@end

@implementation KontagentTracker

@synthesize globalParametersValue = globalParametersValue_;

- (id) initWithKey:(NSString*) key {
    self = [super init];
    if (self) {
        [Kontagent startSession:key mode: kKontagentSDKMode_PRODUCTION];
        KTParamMap* paramMap = [[KTParamMap alloc] init];
        [paramMap put:@"v_maj" value:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
        [Kontagent sendDeviceInformation:paramMap];
    }
    
    return self;
}

- (id) initWithKey:(NSString*) key version:(NSString*) version {
    self = [super init];
    if (self) {
        [Kontagent startSession:key mode: kKontagentSDKMode_PRODUCTION];
        KTParamMap* paramMap = [[KTParamMap alloc] init];
        [paramMap put:@"v_maj" value:version];
        [Kontagent sendDeviceInformation:paramMap];
    }
    
    return self;
}

- (void) setIdentifier:(NSString*) identifier {
    //[TestFlight addCustomEnvironmentInformation:identifier forKey:@"userId"];
}

- (void) setVersion:(NSString*) version {
	//[TestFlight addCustomEnvironmentInformation:version forKey:@"AppVersion"];
}

- (void) setAge:(NSUInteger) age {
    //[TestFlight addCustomEnvironmentInformation:[NSString stringWithFormat:@"%d", age] forKey:@"Age"];
}

- (void) setGender:(LolayTrackerGender) gender {
    if (gender == LolayTrackerGenderMale) {
        //[TestFlight passCheckpoint:@"Gender:Male"];
    } else if (gender == LolayTrackerGenderFemale) {
        //[TestFlight passCheckpoint:@"Gender:Female"];
    } else {
        //[TestFlight passCheckpoint:@"Gender:Unknown"];
    }
}

- (void) setGlobalParameters:(NSDictionary*) globalParameters {
    self.globalParametersValue = [NSMutableDictionary dictionaryWithDictionary:globalParameters];
}

- (void) setGlobalParameter:(NSString*) object forKey:(NSString*) key {
	if (self.globalParametersValue == nil) {
		self.globalParametersValue = [NSMutableDictionary dictionary];
	}
	[self.globalParametersValue setObject:object forKey:key];
}

- (NSString*) machine {
	size_t size;
	int mib[2] = {CTL_HW, HW_MACHINE};
	sysctl(mib, 2, NULL, &size, NULL, 0);
	char* machine = malloc(size);
	sysctl(mib, 2, machine, &size, NULL, 0);
	NSString* machineString = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
	free(machine);
	return machineString;
}

- (KTParamMap*) buildParameters:(NSDictionary*) parameters {
    
    NSMutableDictionary* flurryParameters;
    
    if (parameters == nil) {
        flurryParameters = [[NSMutableDictionary alloc] initWithCapacity:4 + self.globalParametersValue.count];
    } else {
        flurryParameters = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    }
    
    if (self.globalParametersValue) {
        [flurryParameters addEntriesFromDictionary:self.globalParametersValue];
    }
    
    NSString* machine = [self machine];
    NSString* model = [[UIDevice currentDevice] model];
    NSString* systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString* systemName = [[UIDevice currentDevice] systemName];
	NSString* platform = [NSString stringWithFormat:@"%@ (%@): %@ %@", model, machine, systemName, systemVersion];
	NSString* locale = [[NSLocale currentLocale] localeIdentifier];
	NSString* language = [[NSLocale preferredLanguages] objectAtIndex:0];
    [flurryParameters setObject:platform forKey:@"platform"];
    [flurryParameters setObject:locale forKey:@"locale"];
    [flurryParameters setObject:language forKey:@"language"];
	   
    //NSString* paramData = [flurryParameters JSONString];
    NSError* error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:flurryParameters options:0 error:&error];
    if (!jsonData)
        return nil;
    KTParamMap* paramMap = [[KTParamMap alloc] init];
    [paramMap put:@"data" value:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
    return paramMap;
}

- (void) logEvent:(NSString*) name {
    [Kontagent customEvent:name optionalParams:[self buildParameters:nil]];
}

- (void) logEvent:(NSString*) name withDictionary:(NSDictionary*) parameters {
    [Kontagent customEvent:name optionalParams:[self buildParameters:parameters]];
}

- (void) logPage:(NSString*) name {
    [self logEvent:name];
}

- (void) logPage:(NSString*) name withDictionary:(NSDictionary*) parameters {
    [self logEvent:name withDictionary:parameters];
}

- (void) logRevenue:(NSInteger *)valueInCents {
    [Kontagent revenueTracking:valueInCents optionalParams:nil];
}

- (void) logException:(NSException*) exception {
    //TFLog(@"%@", exception.reason);
}

- (void) logError:(NSError*) error {
    //TFLog(@"%@", error.localizedDescription);
}

- (void) stopSession {
    [Kontagent stopSession];
}

@end
