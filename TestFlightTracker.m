//
//  Copyright 2012 Lolay, Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "TestFlightTracker.h"
#import "TestFlight.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@interface TestFlightTracker ()

@property (nonatomic, strong, readwrite) NSMutableDictionary* globalParametersValue;

@end

@implementation TestFlightTracker

@synthesize globalParametersValue = globalParametersValue_;

- (id) initWithKey:(NSString*) key {
    self = [super init];
    if (self) {
        [TestFlight addCustomEnvironmentInformation:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]forKey:@"AppVersion"];
        [TestFlight takeOff:key];
    }
    
    return self;
}

- (id) initWithKey:(NSString*) key version:(NSString*) version {
    self = [super init];
    if (self) {
        [TestFlight addCustomEnvironmentInformation:version forKey:@"AppVersion"];
        [TestFlight takeOff:key];
    }
    
    return self;
}

- (void) setIdentifier:(NSString*) identifier {
    [TestFlight addCustomEnvironmentInformation:identifier forKey:@"userId"];
}

- (void) setVersion:(NSString*) version {
	[TestFlight addCustomEnvironmentInformation:version forKey:@"AppVersion"];
}

- (void) setAge:(NSUInteger) age {
    [TestFlight addCustomEnvironmentInformation:[NSString stringWithFormat:@"%d", age] forKey:@"Age"];
}

- (void) setGender:(LolayTrackerGender) gender {
    if (gender == LolayTrackerGenderMale) {
        [TestFlight passCheckpoint:@"Gender:Male"];
    } else if (gender == LolayTrackerGenderFemale) {
        [TestFlight passCheckpoint:@"Gender:Female"];
    } else {
        [TestFlight passCheckpoint:@"Gender:Unknown"];
    }
}

- (void) setGlobalParameters:(NSDictionary*) globalParameters {
    self.globalParametersValue = [NSMutableDictionary dictionaryWithDictionary:globalParameters];
}

- (void) setGlobalParameter:(NSString*) object forKey:(NSString*) key {
	if (self.globalParametersValue == nil) {
		self.globalParametersValue = [NSMutableDictionary dictionary];
	}
	[self.globalParametersValue setObject:object forKey:key];
}

- (NSString*) machine {
	size_t size;
	int mib[2] = {CTL_HW, HW_MACHINE};
	sysctl(mib, 2, NULL, &size, NULL, 0);
	char* machine = malloc(size);
	sysctl(mib, 2, machine, &size, NULL, 0);
	NSString* machineString = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
	free(machine);
	return machineString;
}

- (NSDictionary*) buildParameters:(NSDictionary*) parameters {
    NSMutableDictionary* flurryParameters;
    
    if (parameters == nil) {
        flurryParameters = [[NSMutableDictionary alloc] initWithCapacity:4 + self.globalParametersValue.count];
    } else {
        flurryParameters = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    }
    
    if (self.globalParametersValue) {
        [flurryParameters addEntriesFromDictionary:self.globalParametersValue];
    }
    
    NSString* machine = [self machine];
    NSString* model = [[UIDevice currentDevice] model];
    NSString* systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString* systemName = [[UIDevice currentDevice] systemName];
	NSString* platform = [NSString stringWithFormat:@"%@ (%@): %@ %@", model, machine, systemName, systemVersion];
	NSString* locale = [[NSLocale currentLocale] localeIdentifier];
	NSString* language = [[NSLocale preferredLanguages] objectAtIndex:0];
    [flurryParameters setObject:platform forKey:@"platform"];
    [flurryParameters setObject:locale forKey:@"locale"];
    [flurryParameters setObject:language forKey:@"language"];
	
	DLog(@"flurryParameters=%@", flurryParameters);
    
    return flurryParameters;
}

- (void) logEvent:(NSString*) name {
    [TestFlight passCheckpoint:name];
}

- (void) logEvent:(NSString*) name withDictionary:(NSDictionary*) parameters {
    [TestFlight passCheckpoint:name];
    for (NSString* key in parameters) {
        if ([key rangeOfString:@"UID"].location == NSNotFound) {
            [TestFlight passCheckpoint:[NSString stringWithFormat:@"%@:%@:%@", name, key, [parameters objectForKey:key]]];
        }
    }
}

- (void) logPage:(NSString*) name {
    [self logEvent:name];
}

- (void) logPage:(NSString*) name withDictionary:(NSDictionary*) parameters {
    [self logEvent:name withDictionary:parameters];
}

- (void) logException:(NSException*) exception {
    TFLog(@"%@", exception.reason);
}

- (void) logError:(NSError*) error {
    TFLog(@"%@", error.localizedDescription);
}

@end
